## Небольшой проект для автоматического рефакторинга сайтов на PHP 8

### Установка

Для установки необходимо выполнить 
```
composer install
```

**Настройка Finder**

Rector внутри использует symfony компонент Finder.
Данный компонент создается стандартным фабричным методом без какой-либо настройки 
и по умолчанию игнорирует скрытые файлы и директории (начинающиеся с символа ```.```), например шаблоны .default.

Чтобы включить данные файлы и директории в обработку, необходимо применить изменения в файле
[./vendor/rector/rector/vendor/symfony/finder/Finder.php](./vendor/rector/rector/vendor/symfony/finder/Finder.php)
, метод ```__construct```

```diff
    public function __construct()
    {
-        $this->ignore = static::IGNORE_VCS_FILES | static::IGNORE_DOT_FILES;
+        $this->ignore = static::IGNORE_VCS_FILES;
    }
```


### Использование

Для работы предлагается использовать созданный базовый набор правил, но с возможностью изменения настроек. 
Для примера приложен файл [rector.project.example.php](./rector.project.example.php), который и нужно разместить в 
корне своего проекта под именем ```rector.php```.

В нем необходимо откорректировать путь к базовому файлу настроек [rector.php](./rector.php), 
определить наследника класса [AbstractRectorBuilder](./rector.php) и вернуть результат метода ```AbstractRectorBuilder::buildConfig```

### Запуск

Для запуска необходимо использовать команду

```php .\..\php8\vendor\bin\rector --config .\rector.php```

Где 
- ```.\..\php8\vendor\bin\rector``` - путь к бинарному файлу ректора, 
- ```.\rector.php``` - путь к файлу настроек правил и директорий для проекта. Все пути относительны текущей директории, из которой производится запуск.

---

### Шаги рефакторинга

> Стандартный рефакторинг заключается в запуске шага **FIX_ALL**, а затем шага **FIX_ALL_WITH_RISKY**

Выделены 4 шага рефакторинга, определенные в перечислении [RectorCustom\RefactorStepEnum](./RectorCustom/RefactorStepEnum.php)

**CHECK_UP_TO83** - включает в себя набор правил ректора ```LevelSetList::UP_TO_PHP_83```, но большинство правил прописано в игнорирование. Необходимо использовать этот шаг только для поиска новых правил, которые могут быть применены.
> Запуск данного шага возможен только с параметром ```--dry-run``` для анализа изменений

**FIX_PHP_83** - включает в себя предварительно отобранный список из стандартных правил ректора, необходимых для применения, см. метод ```getPHP8Rules()```

**FIX_ALL** - включает в себя правила из FIX_PHP_83 и пользовательские правила, см. метод ```getRules()```

**FIX_ALL_WITH_RISKY** - включает в себя правила из FIX_ALL и пользовательские risky правила, см. метод ```getRiskyRules()```
> Запуск данного шага возможен только с параметром ```--dry-run``` для анализа изменений


### Пользовательские правила

Данные правила в основном созданы для работы с массивами и меняют код так, чтобы на входе в функцию был массив, а не null.

**Risky-правила** - данные правила помогают найти код, который, скорее всего, приведет к ошибке. Данный код необходимо изменить вручную.
Правило помечено как risky, если оно может сломать код после его применения, изменения сложно совершить автоматически, либо правило просто недоработано.

---

## Преднастроенные правила

### Стандартные правила ректора для шага **FIX_PHP_83**

\Rector\Php82\Rector\Encapsed\VariableInStringInterpolationFixerRector
<br/> Заменяет "${var}" на "{$var}"

\Rector\Php74\Rector\ArrayDimFetch\CurlyToSquareBracketArrayStringRector
<br/> Заменяет фигурные скобки при доступе к массиву или строке на квадратные

\Rector\Php74\Rector\Ternary\ParenthesizeNestedTernaryRector
<br/> Добавляет скобки для вложенных тернарных операторов

\Rector\Php72\Rector\Unset_\UnsetCastRector
<br/> Удаляет приведение типа к (unset)

\Rector\Php70\Rector\ClassMethod\Php4ConstructorRector
<br/> Заменяет конструктор в стиле PHP 4 на метод __construct.

\Rector\Php70\Rector\FuncCall\EregToPregMatchRector
<br/> Заменяет вызов семейства функций ereg*() на preg*()

\Rector\Php70\Rector\Variable\WrapVariableVariableNameInCurlyBracesRector
<br/> Добавляет фигурные скобки для разыменовывания вложенных переменных ${$foo}

\Rector\CodingStyle\Rector\FuncCall\ConsistentImplodeRector
<br/> Изменяет порядок аргументов в вызове implode на правильный

\Rector\Php70\Rector\StaticCall\StaticCallOnNonStaticToInstanceCallRector
<br/> Заменяет вызов нестатического метода, который вызван как статический

\Rector\Php53\Rector\Variable\ReplaceHttpServerVarsByServerRector
<br/> Меняет устаревшие $HTTP_* переменные на правильные

\Rector\Php72\Rector\Assign\ListEachRector
<br/> Изменяет устаревшую функцию each() на вызов key() и current()

Подробнее по ссылке https://github.com/rectorphp/rector/blob/main/docs/rector_rules_overview.md

### Пользовательские правила ректора для шага **FIX_ALL**

\RectorCustom\Rules\InArrayOnNullRector
<br/> Добавляет приведение типа для второго аргумента к массиву в вызове функции in_array()

\RectorCustom\Rules\CountOnNullRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функций count() и sizeof()

\RectorCustom\Rules\ArrayFilterOnNullRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функции array_filter

\RectorCustom\Rules\ArrayKeyFirstLastOnNullRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функций array_key_first() и array_keys_last()

\RectorCustom\Rules\JoinOnNullRector
<br/> Добавляет приведение типа для второго аргумента к массиву в вызове функций join() и implode()

\RectorCustom\Rules\ArrayKeysValuesOnNullRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функций array_keys() и array_values()

\RectorCustom\Rules\ArrayMergeOnNullRector
<br/> Добавляет приведение типа для всех аргументов к массиву в вызове функции array_merge()

\RectorCustom\Rules\ArrayIntersectOnNullRector
<br/> Добавляет приведение типа для всех аргументов к массиву в вызове функции array_intersect()

\RectorCustom\Rules\ArraySliceOnNullRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функции array_slice()

\RectorCustom\Rules\ArrayFlipOnNullRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функции array_flip()

\RectorCustom\Rules\HttpBuildQueryOnNullRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функции http_build_query()

\RectorCustom\Rules\ArrayPointerOnNullRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функций current() и key()

\RectorCustom\Rules\ArrayDimFetchOnNullRector
<br/> Добавляет оператор объединения с null при получении значения по ключам [VALUE][TEXT] и [~VALUE][TEXT]

\Rector\Transform\Rector\MethodCall\MethodCallToStaticCallRector
<br/> Заменяет нестатические вызовы методов на статические

### Пользовательские risky правила ректора для шага **FIX_ALL_WITH_RISKY**

>Данные правила просто помогают найти проблемные места, которые нужно будет отрефакторить вручную

\RectorCustom\Rules\RiskyArraySortOnNullRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функций sort, asort, ksort, rsort, arsort, krsort, usort, uasort, uksort.
**Ломает код, так как первый аргумент должен быть передан по ссылке**

\RectorCustom\Rules\RiskyResetEndOnNullRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функций reset и end.
**Ломает код, так как первый аргумент должен быть передан по ссылке**

\RectorCustom\Rules\RiskyResetEndOnExplodeRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функций reset и end, если первый аргумент - вызов explode.
**Ломает код, так как первый аргумент должен быть передан по ссылке**

\RectorCustom\Rules\RiskyResetEndOnExplodeRector
<br/> Добавляет приведение типа для первого аргумента к массиву в вызове функций next и prev.
**Ломает код, так как первый аргумент должен быть передан по ссылке**

\RectorCustom\Rules\RiskyValByRefRector
<br/> Добавляет приведение типа аргументов, которые должны быть переданы по ссылке к объекту в вызове функций.
**Ломает код, так как эти аргументы должны быть переданы по ссылке**

### Описание методов и констант базового класса конфигуратора [AbstractRectorBuilder](./rector.php)

```protected const RefactorStepEnum STEP``` - Шаг рефакторинга

```protected const string FOLDER``` - Абсолютный путь к корневой папке для рефакторинга, обычно к public_html

```protected function getRules(): array``` - Возвращает массив используемых пользовательских правил

```protected function getRiskyRules(): array``` - Возвращает массив используемых пользовательских Risky-правил

```protected function getSkipFolders(): array``` - Возвращает массив пропускаемых директорий/файлов

```protected function getSkipRules(): array``` - Возвращает массив пропускаемых правил

```protected function extraConfigure(RectorConfig $rectorConfig): void``` - Метод для гибкой конфигурации

```protected function getPaths(): array``` - Возвращает массив путей для анализа

```protected function getPHP8Rules(): array``` - Возвращает массив используемых стандартных правил ректора

```final protected function getSkipUpToPHP83Rules(): array``` - Исключение стандартных правил для шага CHECK_UP_TO83

```final public static function buildConfig()``` - Создание конфигурации

Базовый класс настроен на стандартный сайт, если нужно что-то свое, то необходимо переопределить соответствующий метод.
Обязательно необходимо переопределять обе константы