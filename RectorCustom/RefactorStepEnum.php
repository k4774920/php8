<?php

namespace RectorCustom;

enum RefactorStepEnum: int {
	case CHECK_UP_TO83 = 10;
	case FIX_PHP_83 = 20;
	case FIX_ALL = 30;
	case FIX_ALL_WITH_RISKY = 40;
}
