<?php
namespace RectorCustom\Rules;

use PhpParser\Node;
use PhpParser\Node\Arg;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\Cast\Array_;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Expr\Ternary;
use PhpParser\Node\Expr\Variable;
use PhpParser\NodeTraverser;
use PHPStan\Analyser\Scope;
use PHPStan\Reflection\ClassReflection;
use PHPStan\Type\ArrayType;
use PHPStan\Type\Type;
use PHPStan\Type\UnionType;
use Rector\NodeAnalyzer\VariableAnalyzer;
use Rector\NodeTypeResolver\Node\AttributeKey;
use Rector\Php\PhpVersionProvider;
use Rector\Rector\AbstractScopeAwareRector;
use Rector\ValueObject\PhpVersionFeature;
use Rector\VersionBonding\Contract\MinPhpVersionInterface;
use Symplify\RuleDocGenerator\ValueObject\RuleDefinition;

use RectorCustom\CountableTypeAnalyzer;
use RectorCustom\CountableAnalyzer;

abstract class AbstractArrayOnNullBase extends AbstractScopeAwareRector implements MinPhpVersionInterface {
	/** Описание правила */
	protected const string RULE_DEFINITION = '';
	/** Массив проверяемых имен функций.
	 * Например count, join
	 */
	abstract protected function getFuncNames(): array;
	/** Массив позиций аргументов, которые должны быть массивом.
	 * Например для count - 0, join - 1, array_merge - все = range(1, 100)
	 */
	abstract protected function getNumArgs(): array;

	protected $countableTypeAnalyzer;
	protected $countableAnalyzer;
	protected $variableAnalyzer;
	protected $phpVersionProvider;

	public function __construct(CountableTypeAnalyzer $countableTypeAnalyzer, CountableAnalyzer $countableAnalyzer, VariableAnalyzer $variableAnalyzer, PhpVersionProvider $phpVersionProvider)
	{
		$this->countableTypeAnalyzer = $countableTypeAnalyzer;
		$this->countableAnalyzer = $countableAnalyzer;
		$this->variableAnalyzer = $variableAnalyzer;
		$this->phpVersionProvider = $phpVersionProvider;
	}

	public function provideMinPhpVersion() : int
	{
		return PhpVersionFeature::COUNT_ON_NULL;
	}

	public function getRuleDefinition() : RuleDefinition
	{
		return new RuleDefinition(static::RULE_DEFINITION, []);
	}

	public function getNodeTypes() : array
	{
		return [FuncCall::class, Ternary::class];
	}

	public function refactorWithScope(Node $node, Scope $scope)
	{
		if ($this->isInsideTrait($scope)) {
			return null;
		}
		if ($node instanceof Ternary) {
			if ($this->shouldSkipTernaryIfElseCountFuncCall($node)) {
				return NodeTraverser::DONT_TRAVERSE_CHILDREN;
			}
			return null;
		}
		if ($this->shouldSkipFuncCall($node)) {
			return null;
		}

		$args = $node->getArgs();
		$hasChanges = false;

		foreach ($this->getNumArgs() as $numArg) {
			if (!isset($args[$numArg])) continue;
			$arg = $args[$numArg];
			$argValue = $arg->value;

			$result = $this->refactorArgument($argValue, $numArg, $scope, $node);
			if ($result !== null) {
				$node = $result;
				$hasChanges = true;
			}
		}

		return $hasChanges ? $node : null;
	}

	protected function refactorArgument(Expr $argValue, int $numArg, Scope $scope, FuncCall $node) {
		if ($this->countableTypeAnalyzer->isCountableType($argValue)) {
			return null;
		}

		$valueType = $this->getType($argValue);
		if ($valueType instanceof ArrayType) {
			return $this->refactorArrayType($argValue, $valueType, $numArg, $scope, $node);
		}

		if ($this->nodeTypeResolver->isNullableTypeOfSpecificType($argValue, ArrayType::class)) {
			return $this->castToArray($argValue, $numArg, $node);
		}

		if ($this->isAlwaysIterableType($valueType)) {
			return null;
		}

		return $this->castToArray($argValue, $numArg, $node);
	}

	protected function isAlwaysIterableType(Type $possibleUnionType) : bool
	{
		if ($possibleUnionType->isIterable()->yes()) {
			return \true;
		}
		if (!$possibleUnionType instanceof UnionType) {
			return \false;
		}
		$types = $possibleUnionType->getTypes();
		foreach ($types as $type) {
			if ($type->isIterable()->no()) {
				return \false;
			}
		}
		return \true;
	}

	protected function castToArray(Expr $argValue, int $numArg, FuncCall $funcCall) : FuncCall
	{
		$castArray = new Array_($argValue);
		$funcCall->args[$numArg] = new Arg($castArray);
		return $funcCall;
	}

	protected function shouldSkipFuncCall(FuncCall $funcCall) : bool
	{
		if (!$this->isNames($funcCall, $this->getFuncNames())) {
			return \true;
		}

		if ($funcCall->isFirstClassCallable()) {
			return \true;
		}

		$args = $funcCall->getArgs();
		foreach ($this->getNumArgs() as $argNum) {
			if (!isset($args[$argNum])) continue;
			$arg = $args[$argNum];

			$origNode = $arg->value->getAttribute(AttributeKey::ORIGINAL_NODE);
			if (!$origNode instanceof Node) {
				return \true;
			}

			if (!$arg->value instanceof Variable) {
				continue;
			}

			if ($this->variableAnalyzer->isStaticOrGlobal($arg->value)) {
				return \true;
			}
		}

		return \false;
	}

	protected function shouldSkipTernaryIfElseCountFuncCall(Ternary $ternary) : bool
	{
		$funcNames = $this->getFuncNames();
		if ($ternary->if instanceof FuncCall && $this->isNames($ternary->if, $funcNames)) {
			return \true;
		}
		return $ternary->else instanceof FuncCall && $this->isNames($ternary->else, $funcNames);
	}

	protected function refactorArrayType(Expr $argValue, ArrayType $arrayType, int $numArg, Scope $scope, FuncCall $funcCall) : ?FuncCall
	{
		if (!$this->countableAnalyzer->isCastableArrayType($argValue, $arrayType, $scope)) {
			return null;
		}
		return $this->castToArray($argValue, $numArg, $funcCall);
	}

	protected function isInsideTrait(Scope $scope) : bool
	{
		$classReflection = $scope->getClassReflection();
		if (!$classReflection instanceof ClassReflection) {
			return \false;
		}
		return $classReflection->isTrait();
	}
}