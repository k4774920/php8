<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use PhpParser\Node;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\ArrayDimFetch;
use \PhpParser\Node\Scalar\String_;
use PHPStan\Analyser\Scope;
use Rector\NodeTypeResolver\Node\AttributeKey;
use Rector\Rector\AbstractScopeAwareRector;
use Rector\ValueObject\Application\File;
use Rector\ValueObject\PhpVersionFeature;
use Rector\VersionBonding\Contract\MinPhpVersionInterface;
use RectorCustom\Token;
use Symplify\RuleDocGenerator\ValueObject\RuleDefinition;

final class ArrayDimFetchOnNullRector extends AbstractScopeAwareRector implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes dim fetch [VALUE][TEXT] and [~VALUE][TEXT] to coalesce';

	public function __construct() {}

	public function getNodeTypes() : array
	{
		return [ArrayDimFetch::class];
	}

	public function refactorWithScope(Node $node, Scope $scope)
	{
		/** @var ArrayDimFetch $node */

		// Если далее идет еще один дим, то пропускаем первый
		// [$a][$b][$c]
		if ($this->isNextDimFetch($this->file, $node)) {
			return null;
		}

		// Если диму идет присваивание, то пропускаем
		// $arr[$a][$b][$c] = xxx
		if ($node->getAttribute(AttributeKey::IS_BEING_ASSIGNED)) {
			return null;
		}

		// Получаем последний дим $arr[$a][$b][$c] => [$c]
		$lastDim = $node->dim;

		// Получаем предыдущий дим $arr[$a][$b][$c] => [$b]
		$prevDim = null;
		if ($node->var instanceof ArrayDimFetch) {
			$prevDim = $node->var->dim;
		}

		// Если дим не строковый, то пропускаем
		// $arr[$a] - Variable
		// $arr[$a ?: 'x'] - Expr
		if (!($lastDim instanceof String_) || !($prevDim instanceof String_)) {
			return null;
		}

		// Строковые димы должны быть со значениями VALUE/~VALUE и TEXT
		if (
			($prevDim->value !== 'VALUE' && $prevDim->value !== '~VALUE')
			||
			($lastDim->value !== 'TEXT')
		) {
			return null;
		}


		// Получаем следующий и предыдущий токены
		$nextToken = $this->getNextToken($this->file, $node->getEndTokenPos());
		$prevToken = $this->getPrevToken($this->file, $node->getStartTokenPos());

		// Если дим является частью Ternary / Coalescing оператора, то пропускам
		// $arr[$a] ?? ...
		// $arr[$a] ?: ...
		// $arr[$a] ? ... : ...
		// ... ?: $arr[$a]
		// ... ? ... : $arr[$a]
		// ... ? $arr[$a] : ...
		if (
			$nextToken->token === '??'
			|| $nextToken->token === '?:'
			|| $nextToken->token === '?'
			|| $prevToken->token === '?:'
			|| $prevToken->token === ':'
			|| ($prevToken->token === '?' && $nextToken->token === ':')
		) {
			return null;
		}

		// Если дим внутри isset, то пропускаем
		// isset($arr[$a])
		if ($nextToken->token === ')' && $prevToken->token === '(') {
			$prevPrevToken = $this->getPrevToken($this->file, $prevToken->pos);
			if ($prevPrevToken->token === 'isset') {
				return null;
			}
		}

		// Рефакторим в Expr\BinaryOp\Coalesce
		// $arr[$a][$b][$c] ?? ''
		return $this->refactorToCoalesce($node);
	}

	protected function refactorToCoalesce(Node $node): Node
	{
		return new Expr\BinaryOp\Coalesce(
			$node,
			new Node\Scalar\String_('')
		);
	}

	public function getRuleDefinition() : RuleDefinition
	{
		return new RuleDefinition(static::RULE_DEFINITION, []);
	}

	public function provideMinPhpVersion(): int
	{
		return PhpVersionFeature::COUNT_ON_NULL;
	}

	private function getTokenByPosStep(File $file, int $tokenPos, int $step): Token
	{
		// skip whitespace tokens
		$oldTokens = $file->getOldTokens();
		do {
			$tokenPos += $step;
			if (!isset($oldTokens[$tokenPos])) {
				$token = null;
				break;
			} else {
				$token = is_array($oldTokens[$tokenPos]) ? $oldTokens[$tokenPos][1] : $oldTokens[$tokenPos];
				if (strlen(trim($token)) > 0) {
					break;
				}
			}
		} while (true);
		return new Token($token, $tokenPos);
	}

	private function getNextToken(File $file, int $pos): Token
	{
		return $this->getTokenByPosStep($file, $pos, 1);
	}

	private function getPrevToken(File $file, int $pos): Token
	{
		return $this->getTokenByPosStep($file, $pos, -1);
	}

	private function isNextDimFetch(File $file, Node $node) : bool
	{
		return $this->getNextToken($file, $node->getEndTokenPos())->token === '[';
	}
}
