<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class ArrayFilterOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes array_filter on null to array cast';
	protected function getFuncNames(): array
	{
		return ['array_filter'];
	}
	protected function getNumArgs(): array
	{
		return [0];
	}
}
