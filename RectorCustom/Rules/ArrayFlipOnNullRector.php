<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class ArrayFlipOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes array_flip on null to array cast';
	protected function getFuncNames(): array
	{
		return ['array_flip'];
	}
	protected function getNumArgs(): array
	{
		return [0];
	}
}
