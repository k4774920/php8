<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class ArrayIntersectOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes array_intersect on null to array cast';
	protected function getFuncNames(): array
	{
		return ['array_intersect'];
	}
	protected function getNumArgs(): array
	{
		return range(0, 100);
	}
}
