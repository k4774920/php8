<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class ArrayKeyFirstLastOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes array_key_first and array_keys_last on null to array cast';
	protected function getFuncNames(): array
	{
		return ['array_key_first', 'array_key_last'];
	}
	protected function getNumArgs(): array
	{
		return [0];
	}
}
