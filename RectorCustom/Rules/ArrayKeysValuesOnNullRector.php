<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class ArrayKeysValuesOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes array_keys() and array_values on null to array cast';
	protected function getFuncNames(): array
	{
		return ['array_keys', 'array_values'];
	}
	protected function getNumArgs(): array
	{
		return [0];
	}
}
