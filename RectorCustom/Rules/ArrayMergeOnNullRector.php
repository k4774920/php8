<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class ArrayMergeOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes array_merge() on null to array cast';
	protected function getFuncNames(): array
	{
		return ['array_merge'];
	}
	protected function getNumArgs(): array
	{
		return range(0, 100);
	}
}
