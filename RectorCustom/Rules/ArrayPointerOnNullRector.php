<?php
declare (strict_types=1);

namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class ArrayPointerOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface {
	protected const string RULE_DEFINITION = 'Changes current and key on null to array cast';

	protected function getFuncNames(): array
	{
		return ['current', 'key'];
	}

	protected function getNumArgs(): array
	{
		return [0];
	}
}
