<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class ArraySliceOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes array_slice() on null to array cast';
	protected function getFuncNames(): array
	{
		return ['array_slice'];
	}
	protected function getNumArgs(): array
	{
		return [0];
	}
}
