<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class CountOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes count() and sizeof() on null to array cast';
	protected function getFuncNames(): array
	{
		return ['count', 'sizeof'];
	}
	protected function getNumArgs(): array
	{
		return [0];
	}
}
