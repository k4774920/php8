<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class HttpBuildQueryOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes http_build_query() on null to array cast';
	protected function getFuncNames(): array
	{
		return ['http_build_query'];
	}
	protected function getNumArgs(): array
	{
		return [0];
	}
}
