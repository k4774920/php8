<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class InArrayOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes in_array() on null to array cast';
	protected function getFuncNames(): array
	{
		return ['in_array'];
	}
	protected function getNumArgs(): array
	{
		return [1];
	}
}
