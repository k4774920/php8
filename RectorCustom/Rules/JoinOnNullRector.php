<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;


final class JoinOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes join() and implode() on null to array cast';
	protected function getFuncNames(): array
	{
		return ['join', 'implode'];
	}
	protected function getNumArgs(): array
	{
		return [1];
	}
}
