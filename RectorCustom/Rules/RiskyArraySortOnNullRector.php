<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;

/**
 * Исправленный данным правилом вариант sort((array) $var) не является корректным.
 * Данный код будет вызывать ошибку Argument #1 cannot be passed by reference
 *
 * Данное правило просто помогает найти проблемные места, которые нужно будет отрефакторить вручную
 */

final class RiskyArraySortOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes all array sort on null to array cast: sort, asort, ksort, rsort, arsort, krsort, usort, uasort, uksort';
	protected function getFuncNames(): array
	{
		return ['sort', 'asort', 'ksort', 'rsort', 'arsort', 'krsort', 'usort', 'uasort', 'uksort'];
	}
	protected function getNumArgs(): array
	{
		return [0];
	}
}
