<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use PhpParser\Node\Expr;
use PhpParser\Node\Expr\FuncCall;
use PHPStan\Analyser\Scope;
use Rector\VersionBonding\Contract\MinPhpVersionInterface;

/**
 * Исправленный данным правилом вариант reset((array) explode(...)) не является корректным.
 *
 * Данное правило просто помогает найти проблемные места, которые нужно будет отрефакторить вручную
 */

final class RiskyResetEndOnExplodeRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes all reset end from explode';
	protected function getFuncNames(): array
	{
		return ['reset', 'end'];
	}
	
	protected function getNumArgs(): array
	{
		return [0];
	}

	protected function refactorArgument(Expr $argValue, int $numArg, Scope $scope, FuncCall $node) {
		if ($argValue instanceof FuncCall && $this->isName($argValue, 'explode')) {
			return $this->castToArray($argValue, $numArg, $node);
		}
		return null;
	}
}
