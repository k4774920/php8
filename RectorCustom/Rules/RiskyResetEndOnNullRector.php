<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use Rector\VersionBonding\Contract\MinPhpVersionInterface;

/**
 * Исправленный данным правилом вариант reset((array) $var) не является корректным.
 * Данный код будет вызывать ошибку Argument #1 cannot be passed by reference
 *
 * Данное правило просто помогает найти проблемные места, которые нужно будет отрефакторить вручную
 */

final class RiskyResetEndOnNullRector extends AbstractArrayOnNullBase implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes reset() and end() on null to array cast';
	protected function getFuncNames(): array
	{
		return ['reset', 'end'];
	}
	protected function getNumArgs(): array
	{
		return [0];
	}
}
