<?php

declare (strict_types=1);
namespace RectorCustom\Rules;

use PhpParser\Node;
use PhpParser\Node\Arg;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\Cast\Object_;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Expr\Variable;
use PHPStan\Analyser\Scope;
use Rector\Rector\AbstractScopeAwareRector;
use Rector\ValueObject\PhpVersionFeature;
use Rector\VersionBonding\Contract\MinPhpVersionInterface;
use Symplify\RuleDocGenerator\ValueObject\RuleDefinition;

/**
 * Исправленный данным правилом вариант func((object) $var) не является корректным.
 * Данный код будет вызывать ошибку Argument #1 cannot be passed by reference
 *
 * Данное правило просто помогает найти проблемные места, которые нужно будет отрефакторить вручную
 */

final class RiskyValByRefRector extends AbstractScopeAwareRector implements MinPhpVersionInterface
{
	protected const string RULE_DEFINITION = 'Changes value to object variable cast';
	private function getFunctions(): array
	{
		return [
			'CFormResult::GetDataByID' => [2, 3],
			'CForm::GetDataByID' => [1, 2, 3, 4, 5],
			'CFile::ResizeImageFile' => [1],
		];
	}
	protected function getFuncNames(): array
	{
		return array_keys($this->getFunctions());
	}
	protected function getNumArgs($stMethod): array
	{
		$numArgs = $this->getFunctions();
		return $numArgs[$stMethod] ?? [];
	}

	public function getNodeTypes() : array
	{
		return [StaticCall::class];
	}

	public function refactorWithScope(Node $node, Scope $scope)
	{
		if ($node->name instanceof Expr) {
			return null;
		}
		$methodName = $this->getName($node->name);
		$className = $this->resolveStaticCallClassName($node);
		if ($methodName === null) {
			return null;
		}
		if ($className === null) {
			return null;
		}

		$stMethod = $className.'::'.$methodName;

		if ($this->shouldSkipFuncCall($node, $stMethod)) {
			return null;
		}

		$args = $node->getArgs();
		$hasChanges = false;

		foreach ($this->getNumArgs($stMethod) as $numArg) {
			if (!isset($args[$numArg])) continue;
			$arg = $args[$numArg];
			$argValue = $arg->value;

			$result = $this->refactorArgument($argValue, $numArg, $scope, $node);
			if ($result !== null) {
				$node = $result;
				$hasChanges = true;
			}
		}

		return $hasChanges ? $node : null;
	}

	protected function refactorArgument(Expr $argValue, int $numArg, Scope $scope, StaticCall $node) {
		if ($argValue instanceof Variable) {
			return null;
		}

		$cast = new Object_($argValue);
		$node->args[$numArg] = new Arg($cast);
		return $node;
	}

	private function resolveStaticCallClassName(StaticCall $staticCall) : ?string
	{
		if ($staticCall->class instanceof PropertyFetch) {
			$objectType = $this->getType($staticCall->class);
			if ($objectType instanceof ObjectType) {
				return $objectType->getClassName();
			}
		}
		return $this->getName($staticCall->class);
	}

	protected function shouldSkipFuncCall(StaticCall $funcCall, $stMethod) : bool
	{
		if (!in_array($stMethod, $this->getFuncNames())) {
			return \true;
		}

		if ($funcCall->isFirstClassCallable()) {
			return \true;
		}

		$args = $funcCall->getArgs();
		foreach ($this->getNumArgs($stMethod) as $argNum) {
			if (!isset($args[$argNum])) continue;
			$arg = $args[$argNum];
			if (!$arg->value instanceof Variable) {
				return false;
			}
		}

		return true;
	}

	public function getRuleDefinition() : RuleDefinition
	{
		return new RuleDefinition(static::RULE_DEFINITION, []);
	}

	public function provideMinPhpVersion(): int
	{
		return PhpVersionFeature::COUNT_ON_NULL;
	}
}
