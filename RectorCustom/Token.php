<?php

namespace RectorCustom;

readonly class Token {
	public function __construct(
		public ?string $token,
		public int $pos,
	) {}
}