<?php
declare(strict_types=1);

use Rector\Config\RectorConfig;
use RectorCustom\RefactorStepEnum;
use Rector\Set\ValueObject\LevelSetList;

/**
 * Компонент Finder внутри ректора по умолчанию игнорирует файлы и папки, которые начинаются с .
 * Параметра, который настраивает это поведение вроде нет.
 *
 * Изменить vendor/rector/rector/vendor/symfony/finder/Finder.php : __construct
 */

/**
 * composer install
 * D:\php\php-8.3.3-nts-Win32-vs16-x64\php.exe .\..\php8\vendor\bin\rector --config .\rector.php --dry-run
 */

abstract class AbstractRectorBuilder {
	// Шаг рефакторинга
	protected const RefactorStepEnum STEP = RefactorStepEnum::CHECK_UP_TO83;

	// Абсолютный путь к корневой папке для рефакторинга, обычно к public_html
	protected const string FOLDER = '';

	protected function __construct() {}

	// массив путей для анализа
	protected function getPaths(): array
	{
		return [$this::FOLDER];
	}

	// массив стандартных правил ректора
	protected function getPHP8Rules(): array
	{
		return [
			\Rector\Php82\Rector\Encapsed\VariableInStringInterpolationFixerRector::class,
			\Rector\Php74\Rector\ArrayDimFetch\CurlyToSquareBracketArrayStringRector::class,
			\Rector\Php74\Rector\Ternary\ParenthesizeNestedTernaryRector::class,
			\Rector\Php72\Rector\Unset_\UnsetCastRector::class,
			\Rector\Php70\Rector\ClassMethod\Php4ConstructorRector::class,
			\Rector\Php70\Rector\FuncCall\EregToPregMatchRector::class,
			\Rector\Php70\Rector\Variable\WrapVariableVariableNameInCurlyBracesRector::class,
			\Rector\CodingStyle\Rector\FuncCall\ConsistentImplodeRector::class,
			\Rector\Php70\Rector\StaticCall\StaticCallOnNonStaticToInstanceCallRector::class,
			//\Rector\Php73\Rector\ConstFetch\SensitiveConstantNameRector::class,
			\Rector\Php53\Rector\Variable\ReplaceHttpServerVarsByServerRector::class,
			\Rector\Php72\Rector\Assign\ListEachRector::class,
		];
	}

	// массив используемых пользовательских правил
	protected function getRules(): array
	{
		return [
			\RectorCustom\Rules\InArrayOnNullRector::class,
			\RectorCustom\Rules\CountOnNullRector::class,
			\RectorCustom\Rules\ArrayFilterOnNullRector::class,
			\RectorCustom\Rules\ArrayKeyFirstLastOnNullRector::class,
			\RectorCustom\Rules\JoinOnNullRector::class,
			\RectorCustom\Rules\ArrayKeysValuesOnNullRector::class,
			\RectorCustom\Rules\ArrayMergeOnNullRector::class,
			\RectorCustom\Rules\HttpBuildQueryOnNullRector::class,
			\RectorCustom\Rules\ArrayIntersectOnNullRector::class,
			\RectorCustom\Rules\ArraySliceOnNullRector::class,
			\RectorCustom\Rules\ArrayFlipOnNullRector::class,
			\RectorCustom\Rules\ArrayDimFetchOnNullRector::class,
			\RectorCustom\Rules\ArrayPointerOnNullRector::class,

			//bitrix
			\Rector\Transform\Rector\MethodCall\MethodCallToStaticCallRector::class => [
				new \Rector\Transform\ValueObject\MethodCallToStaticCall('CFormResult', 'GetDataByID', 'CFormResult', 'GetDataByID'),
				new \Rector\Transform\ValueObject\MethodCallToStaticCall('CFormResult', 'GetList', 'CFormResult', 'GetList'),
			]
		];
	}

	// массив используемых пользовательских Risky-правил
	protected function getRiskyRules(): array
	{
		return [
			\RectorCustom\Rules\RiskyArraySortOnNullRector::class,
			\RectorCustom\Rules\RiskyResetEndOnExplodeRector::class,
			\RectorCustom\Rules\RiskyResetEndOnNullRector::class,
			\RectorCustom\Rules\RiskyArrayPointerOnNullRector::class,

			//bitrix
			\RectorCustom\Rules\RiskyValByRefRector::class
		];
	}

	// массив пропускаемых директорий/файлов
	protected function getSkipFolders(): array
	{
		return [
			$this::FOLDER . '/bitrix/components/bitrix',
			$this::FOLDER . '/upload',
			$this::FOLDER . '/*/modules/techdir.sentry/*',
			$this::FOLDER . '/local/vendor/',
			$this::FOLDER . '/local/php_interface/composer/vendor',
			$this::FOLDER . '/local/modules/*/vendor/*',
			$this::FOLDER . '/bitrix/*/tcpdf/*',
			$this::FOLDER . '/local/*/tcpdf/*',
		];
	}

	// массив пропускаемых правил
	protected function getSkipRules(): array
	{
		return [];
	}

	// дополнительная настройка
	protected function extraConfigure(RectorConfig $rectorConfig): void
	{ }

	// исключение стандартных правил LevelSetList::UP_TO_PHP_83
	final protected function getSkipUpToPHP83Rules(): array
	{
		return [
			// Эти правила не особо важны и не всегда равноценная замена по коду (например switch to match)
			// большинство правил меняет код так, что он будет работать только на PHP 8
			\Rector\Php54\Rector\Array_\LongArrayToShortArrayRector::class,
			\Rector\Php80\Rector\NotIdentical\StrContainsRector::class,
			\Rector\Php80\Rector\Identical\StrEndsWithRector::class,
			\Rector\Php80\Rector\Identical\StrStartsWithRector::class,
			\Rector\Php55\Rector\Class_\ClassConstantToSelfClassRector::class,
			\Rector\Php71\Rector\ClassConst\PublicConstantVisibilityRector::class,
			\Rector\Php80\Rector\FuncCall\ClassOnObjectRector::class,
			\Rector\Php53\Rector\FuncCall\DirNameFileConstantToDirConstantRector::class,
			\Rector\Php83\Rector\ClassMethod\AddOverrideAttributeToOverriddenMethodsRector::class,
			\Rector\Php52\Rector\Switch_\ContinueToBreakInSwitchRector::class,
			\Rector\Php71\Rector\FuncCall\RemoveExtraParametersRector::class,
			\Rector\Php80\Rector\Catch_\RemoveUnusedVariableInCatchRector::class,
			\Rector\Php80\Rector\FunctionLike\MixedTypeRector::class,
			\Rector\Php56\Rector\FuncCall\PowToExpRector::class,
			\Rector\Php83\Rector\ClassConst\AddTypeToConstRector::class,
			\Rector\Php81\Rector\Property\ReadOnlyPropertyRector::class,
			\Rector\Php80\Rector\Class_\ClassPropertyAssignToConstructorPromotionRector::class,
			\Rector\Php74\Rector\Closure\ClosureToArrowFunctionRector::class,
			\Rector\Arguments\Rector\FuncCall\FunctionArgumentDefaultValueReplacerRector::class,
			\Rector\Php81\Rector\Array_\FirstClassCallableRector::class,
			\Rector\TypeDeclaration\Rector\ClassMethod\ReturnNeverTypeRector::class,
			\Rector\Php71\Rector\TryCatch\MultiExceptionCatchRector::class,
			\Rector\Php80\Rector\Switch_\ChangeSwitchToMatchRector::class,
			\Rector\DeadCode\Rector\StaticCall\RemoveParentCallWithoutParentRector::class,

			// Эти правила сделают код более красивым, применять по желанию в настройках для проекта
			// Данные правила можно безопасно применять на php 7
			\Rector\Php70\Rector\Ternary\TernaryToNullCoalescingRector::class,
			\Rector\Php53\Rector\Ternary\TernaryToElvisRector::class,
			\Rector\Php74\Rector\Assign\NullCoalescingOperatorRector::class,
			\Rector\Php70\Rector\FuncCall\MultiDirnameRector::class,
			\Rector\Php80\Rector\Class_\StringableForToStringRector::class,
			\Rector\Php71\Rector\List_\ListToArrayDestructRector::class,
			\Rector\Php55\Rector\String_\StringClassNameToClassConstantRector::class,
			\Rector\Php70\Rector\FuncCall\RandomFunctionRector::class,
			\Rector\Php73\Rector\FuncCall\SetCookieRector::class,
			\Rector\Php52\Rector\Property\VarToPublicPropertyRector::class,
			\Rector\Php80\Rector\ClassMethod\FinalPrivateToPrivateVisibilityRector::class,
			\Rector\Php70\Rector\If_\IfToSpaceshipRector::class,
			\Rector\Php73\Rector\FuncCall\ArrayKeyFirstLastRector::class,
			\Rector\Php74\Rector\Property\RestoreDefaultNullToNullableTypePropertyRector::class,

			// Это правило нужное, но применять его опасно на битриксе
			\Rector\CodeQuality\Rector\ClassMethod\OptionalParametersAfterRequiredRector::class,

			// Тоже нужные правила, но не для битрикса - много изменений
			\Rector\Php81\Rector\FuncCall\NullToStrictStringFuncCallArgRector::class,
			\Rector\Php73\Rector\FuncCall\StringifyStrNeedlesRector::class,

			// Обязательные правила, применяются в getPHP8Rules
			// Данные правила можно безопасно применять на php 7
			\Rector\Php82\Rector\Encapsed\VariableInStringInterpolationFixerRector::class,
			\Rector\Php74\Rector\ArrayDimFetch\CurlyToSquareBracketArrayStringRector::class,
			\Rector\Php74\Rector\Ternary\ParenthesizeNestedTernaryRector::class,
			\Rector\Php72\Rector\Unset_\UnsetCastRector::class,
			\Rector\Php70\Rector\ClassMethod\Php4ConstructorRector::class,
			\Rector\Php70\Rector\FuncCall\EregToPregMatchRector::class,
			\Rector\Php70\Rector\Variable\WrapVariableVariableNameInCurlyBracesRector::class,
			\Rector\CodingStyle\Rector\FuncCall\ConsistentImplodeRector::class,
			\Rector\Php70\Rector\StaticCall\StaticCallOnNonStaticToInstanceCallRector::class,
			\Rector\Php73\Rector\ConstFetch\SensitiveConstantNameRector::class,
			\Rector\Php53\Rector\Variable\ReplaceHttpServerVarsByServerRector::class,
			\Rector\Php72\Rector\Assign\ListEachRector::class,
		];
	}

	// создание конфигурации
	final public static function buildConfig() {
		$instance = new static();
		if ($instance::STEP->value != RefactorStepEnum::FIX_PHP_83->value && $instance::STEP->value != RefactorStepEnum::FIX_ALL->value) {
			if (!in_array('--dry-run', $GLOBALS['argv'])) {
				die('Run in this mode only with --dry-run');
			}
		}
		return static function (RectorConfig $rectorConfig) use ($instance) : void {
			$rectorConfig->parallel(360);
			$rectorConfig->paths($instance->getPaths());

			if ($instance::STEP->value >= RefactorStepEnum::FIX_PHP_83->value) {
				$rules = [];
				if ($instance::STEP->value >= RefactorStepEnum::FIX_PHP_83->value) $rules = array_merge($rules, $instance->getPHP8Rules());
				if ($instance::STEP->value >= RefactorStepEnum::FIX_ALL->value) $rules = array_merge($rules, $instance->getRules());
				if ($instance::STEP->value >= RefactorStepEnum::FIX_ALL_WITH_RISKY->value) $rules = array_merge($rules, $instance->getRiskyRules());

				foreach ($rules as $k => $v) {
					if (is_array($v)) {
						$rectorConfig->ruleWithConfiguration($k, $v);
					} else {
						$rectorConfig->rule($v);
					}
				}

				$rectorConfig->skip(
					array_merge(
						$instance->getSkipFolders(),
						$instance->getSkipRules()
					)
				);
			} else {
				$rectorConfig->sets([LevelSetList::UP_TO_PHP_83]);
				$rectorConfig->skip(array_merge(
					$instance->getSkipFolders(),
					$instance->getSkipUpToPHP83Rules()
				));
			}

			if ($instance::STEP->value >= RefactorStepEnum::FIX_ALL->value) {
				$instance->extraConfigure($rectorConfig);
			}
		};
	}
}