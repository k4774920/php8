<?php
declare(strict_types=1);

// D:\php\php-8.3.3-nts-Win32-vs16-x64\php.exe .\..\php8\vendor\bin\rector --config .\rector.php --dry-run

/**
 * ��������� Finder ������ ������� �� ��������� ���������� ����� � �����, ������� ���������� � .
 * ���������, ������� ����������� ��� ��������� ����� ���.
 *
 * �������� vendor/rector/rector/vendor/symfony/finder/Finder.php : __construct
 * * $this->ignore = static::IGNORE_VCS_FILES;
 */

use RectorCustom\RefactorStepEnum;

// ���� � �������� ����� ��������
require __DIR__ . '/../php8/rector.php';

class RectorBuilder extends AbstractRectorBuilder {
	protected const RefactorStepEnum STEP = RefactorStepEnum::FIX_ALL;
	protected const string FOLDER = __DIR__ . '/public_html';
	protected function getSkipFolders(): array
	{
		return array_merge(
			parent::getSkipFolders(),
			[
				$this::FOLDER . '/bitrix/modules',
			]
		);
	}
}

return RectorBuilder::buildConfig();